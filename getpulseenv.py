
import os
import subprocess as sbp
from os import listdir
from os.path import isfile, join, isdir
import numpy as np



def find_ctr_par(x,y,Eff=None):
  assert(len(x)==3)
  assert(len(y)==3)
  
  w=np.ones_like(x)
  f=np.zeros_like(y)
  X=np.zeros_like(x)
  u=np.ones_like(x)
  A=np.zeros_like(y)
  
  for ix,xv in enumerate(x,0):
    for j in np.arange(3):
      if j==ix:
        continue
      w[ix]*=xv-x[j]
      X[ix]+=x[j]
      u[ix]*=x[j]
    f[ix]=y[ix]/w[ix]

  A[0]=np.dot(f,u);   A[1]=-np.dot(f,X) ;  A[2]=np.sum(f)
  
  Ac=A[2]; xc=-A[1]/(2.0*A[2]); yc=A[0]-A[1]**2/(4.0*A[2])
  if Eff is None:
    Dxc=np.sqrt(-0.5*yc/Ac )
  elif Eff :
    Dxc=np.sqrt(-0.5*yc/Ac )
  elif not Eff:
    lmbDer=(y[2]-y[1])/(x[2]-x[1])
    lmbIz=-(y[1]-y[0])/(x[1]-x[0])
    lmb=0.5*(lmbDer+lmbIz)/Ac
    Dxc=1/lmb
  return xc,yc,Dxc


def find_localext(vec,ithresh=1):
  assert(ithresh>=1)
  vabs=np.abs(vec)
  mxLs=[]
  sgn=[]
  mxneigh=[]
  truenigh=[]
  imxlocLs=[]
  
  for iv,v in enumerate(vabs[ithresh:-ithresh],1):
    pass
    if v>vabs[iv-1] and v>vabs[iv+1]:
      mxLs.append(v)
      imxlocLs.append(iv)
      sgn.append(vec[iv]/v)
      mxneigh.append([vabs[iv-1] ,v,vabs[iv+1]])
      truenigh.append([vec[iv-1],vec[iv],vec[iv+1]])
      
  print("Found ", len(mxLs), " extremes")
  return imxlocLs,sgn,mxLs

def infer_local_maxima(xvec,yvec):#,imxlocLs):
  assert(len(xvec)==len(yvec))
  
  imxLs,sgnLs,mxLs=find_localext(yvec)
  
  
  xadd=[];yadd=[];yaddsgn=[];
  xorig=[];yorig=[];yorigsgn=[];
  for i0,imx in enumerate(imxLs):
    x3=[xvec[imx-1],xvec[imx],xvec[imx+1]]
    y3=[yvec[imx-1],yvec[imx],yvec[imx+1]]
    if len(list(set(x3 )))!=3 :
      print("Skipped x3 set with repeated value")
      continue
    xc,yc,Dxc=find_ctr_par(x3,y3,Eff=None)
    xadd.append(xc)
    yaddsgn.append(yc)
    yadd.append(abs(yc))
  #return iloc, xvals,yvals
    xorig.append( xvec[imx])
    yorig.append( abs(yvec[imx]))
    yorigsgn.append( yvec[imx])
    
  xfull0=xorig+xadd
  yfull=yorig+yadd
  yfullsgn=yorigsgn+yaddsgn
  xfull=sorted(xfull0)
  idx=[ xfull0.index(x00) for x00 in xfull ]#[0]*(len(xfull))
  #idx.sort(key=lambda xfull: input[xfull])
  yfull=[yfull[i] for i in idx ]
  yfullsgn=[yfullsgn[i] for i in idx ]
  #yfull
  
  return xadd,yadd,yaddsgn,xfull,yfull,yfullsgn
#def find_all_extrema():
  
  
####################################################################
#  pass

if __name__=="__main__":

    pulse0dir="pulseoutTDSE/"
    pulseout="inferred_envs/"
    ext=".dat"

    if isdir(pulseout)==False:
        cmdstr="mkdir "+pulseout
        sbp.call(cmdstr,shell=True)

    pulse0Ls=[f for f in listdir(pulse0dir) if isfile(pulse0dir+f) if ext in f]
    print("Found the following pulseout files", pulse0Ls)
    #pulse0=np.loadtxt(

    for pulse0 in pulse0Ls:
        print("Processing : ", pulse0)
        pls=np.loadtxt(pulse0dir+pulse0)
        #find_localext(pls[:,1],ithresh=1)
        xadd,yadd,yaddsgn,xfull,yfull,yfullsgn=infer_local_maxima(pls[:,0],pls[:,1])
        
        np.savetxt(pulseout+"infer_"+pulse0,np.transpose([xadd,yadd ]) )
        np.savetxt(pulseout+"full_"+pulse0,np.transpose([xfull,yfull]) )
        #xorig,yorig,ysgnorig=
        
    #np.savetxt("allmx_"+pulse0,np.transpose([,]  ) )
    
    
    #xfull=np.concatenate(pls[:,0],np.asarray(xadd))
    #ifull=np.argsort(xfull)
    #yfull=np.concatenate(pls[:,1],np.asarray(yadd) )
    #yfull=yfull[ifull]
    
    

