# Infer local maxima

## General description 

It is often the case that an oscillating signal is sampled with enough resolution to correctly characterize it, well into safe territory upwards of the Nyquist limit. However, if the precise location of a local maxima is needed, the apparently sufficient sampling may not be appropriate. In other words, it requires some luck (or a very dense grid) to have the exact maximum of the continuous curve occur at exactly one sample point. 

That was the starting point for this short project. Assuming the sampling is good enough, such that the original --and only known through the samples-- function has no oscillations and a slight curvature between two points, we propose the following lagrangian interpolant approach. Three points define a quadratic function that interpolates them all. From this parabola we analytically obtain its maximum value and its position in the independent variable. We also produce a measure of its width. 
That is what the method find_ctr_par produces, and it could be used independently. In this application we apply it to finding the envelope of a pulse-type function. 

Given a signal function we parse it to obtain the local maxima and minima by getting the local maxima of its absolute value throghout the grid. As the above describe method needs three distinct points, we check whether there are repeated independent variable points, which would make the math ill defined. 
For each local maximum (in absolute value) we extrapolate the true position and value of the extrema, later to be exported. This implementation produces two exports: one with both the inferred and the available local maximal amplitudes and another one with just the analytically extrapolated ones. 

The supplied figure shows the original pulse and the analytically obtained envelope, and was made with xmgrace, so no plotting script is included here. 

## Methods

+ find_ctr_par(x, y, Eff=None): This function takes in three arguments: x, y, and Eff. x and y are one-dimensional arrays of three elements each. Eff is an optional boolean parameter that indicates if the function should compute the effective width of the peak. If Eff is None or True, the function computes the effective width of the peak. If Eff is False, the function computes the full width at half maximum. The function calculates the parameters of a parabola that fits the three points defined by x and y. The function returns the x-coordinate of the vertex of the parabola, the y-coordinate of the vertex of the parabola, and the effective or full width of the peak, depending on the value of Eff.

+ find_localext(vec, ithresh=1): This function takes in two arguments: vec and ithresh. vec is a one-dimensional array. ithresh is an optional integer parameter that indicates the threshold for identifying an extreme point. The function finds all the extreme points of vec and returns their indices, signs, and absolute values.

+ infer_local_maxima(xvec, yvec): This function takes in two arguments: xvec and yvec. xvec and yvec are one-dimensional arrays of the same length. The function infers the location of the local maxima in the curve defined by xvec and yvec by fitting parabolas to the three points around each extreme point of yvec. The function returns the x-coordinates, absolute values, and signs of the inferred local maxima, as well as the x-coordinates, absolute values, and signs of all the local maxima.

The code that runs the functions reads in a set of data files (inside folder pulseoutTDSE), processes each data file, and writes the inferred local maxima and full curve to output files (into inferred_envs). 
